<?php
/**
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 16.05.2018
 * Time: 16:07
 */

$titre = "Hâpy - confirm création de compte";
// ouvre la mémoire tampon
ob_start();
?>
    <main id="authentication" class="inner-bottom-md">
        <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
            <div class="row">
                <div class="col-md-6">
                    <section class="section sign-in inner-right-xs">
                        <h3>Votre Compte a bien été créé </h3>
                    </section>
                </div>
            </div>
        </div>
    </main>
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>