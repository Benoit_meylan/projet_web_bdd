<?php
/**
 * Created by PhpStorm.
 * User: MEYLANBenoit
 * Date: 11.05.2018
 * Time: 23:01
 */

$titre = "Hâpy - déconnexion";
// ouvre la mémoire tampon
ob_start();

session_destroy();
?>
<script>
    window.onload = function() {
        if(!window.location.hash) {
            window.location = window.location + '#loaded';
            window.location.reload();
        }
    }
</script>

<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-6">
                <section class="section sign-in inner-right-xs">
                    <h3>Vous êtes bien déconnecté</h3>
                </section>
            </div>
        </div>
    </div>
</main>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
