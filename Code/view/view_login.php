<?php
/**
 * Created by PhpStorm.
 * User: leo.Zmoos
 * Date: 13.03.2018
 * Time: 08:45
 */


$titre = "hapy - login";
// ouvre la mémoire tampon
ob_start();
// grab recaptcha library
?>
<br>
<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-6">
                <section class="section sign-in inner-right-xs">
                    <h2 class="bordered">Connexion</h2>
                    <p>Bonjour, bienvenue sur votre compte</p>
                    <?php if(isset($_GET['errLoginVide'])) : ?>
                        <p class="text-danger">champ vide !</p>
                    <?php endif; ?>
                    <?php if(isset($_GET['errLogin'])) : ?>
                        <p class="text-danger">vous avez fait une erreur !</p>
                    <?php endif; ?>
                    <form role="form" class="form-group" method="post" action="index.php?action=signin">
                        <div class="field-row">
                            <label>Email</label>
                            <input type="text" class="form-control" name="mail_sign" value="<?php if (isset($_GET['qMail'])) echo $_GET['qMail'];?>" required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Password</label>
                            <input type="password" class="form-control" name="password_sign" required>
                        </div><!-- /.field-row -->
                        <br>
                        <div class="field-row clearfix">
                            <span class="pull-left">
                                <a href="index.php?action=view_recover_password" class="content-color bold"><font color="black">Mot de passe oublié ?</font></a>
                            </span>
                        </div>
                        <br>

                        <div class="buttons-holder">
                            <table class="table">
                                <tr>
                                    <td><button type="submit" class="btn btn-default" style="background-color: lightgrey">Connexion</button></td>
                                    <!-- LOGIN FACEBOOK (NON FONCTIONNEL) <td><div class="fb-login-button" data-width="100" data-max-rows="1" data-size="large" data-button-type="login_with" data-show-faces="false" data-auto-logout-link="false" data-use-continue-as="false"></div></td>-->
                                </tr>
                            </table>
                        </div><!-- /.buttons-holder -->
                    </form><!-- /.cf-style-1 -->
                </section><!-- /.sign-in -->
            </div><!-- /.col -->

            <div class="col-md-6">
                <section class="section register inner-left-xs">
                    <h2 class="bordered">Créer un compte</h2>
                    <p>Inscrivez-vous gratuitement sur notre site</p>
                    <?php if(isset($_GET['errPassword'])) : ?>
                        <p class="text-danger">erreur de mot de passe</p>
                    <?php endif; ?>
                    <?php if(isset($_GET['errMail'])) :?>
                        <p class="text-danger">adresse mail non disponible</p>
                    <?php endif; ?>
                    <?php if(isset($_GET['errCaptcha'])):?>
                        <p class="text-danger">adresse mail non disponible</p>
                    <?php endif;?>

                    <!----Formulaire de création de compte -->
                    <form role="form" class="form-group" method="post" action="index.php?action=addUser">
                        <div class="field-row">
                            <label>Civilité</label>
                            <select class="form-control" name="civilite" required>
                                <option value="m">M.</option>
                                <option value="mme">Mme</option>
                            </select>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Prénom</label>
                            <input type="text" class="form-control" name="prenom" value="<?php if (isset($_GET['qPrenom'])) echo $_GET['qPrenom'];?>" required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Nom</label>
                            <input type="text" class="form-control" name="nom" value="<?php if (isset($_GET['qNom'])) echo $_GET['qNom'];?>" required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Email</label>
                            <input type="email" class="form-control" name="mail" value="<?php if (isset($_GET['qMail'])) echo $_GET['qMail'];?>"  required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>N° Téléphone</label>
                            <input type="number" class="form-control" name="ntel" value="<?php if (isset($_GET['qTel'])) echo $_GET['qTel'];?>" required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Adresse</label>
                            <input type="text" class="form-control" name="adresse" value="<?php if (isset($_GET['qAdresse'])) echo $_GET['qAdresse'];?>" required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Ville</label>
                            <input type="text" class="form-control" name="ville" value="<?php if (isset($_GET['qVille'])) echo $_GET['qVille'];?>" required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>NPA</label>
                            <input type="number" class="form-control" name="npa" value="<?php if (isset($_GET['qNpa'])) echo $_GET['qNpa'];?>" required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Pays</label>
                            <select name='pays' class="form-control" required>
                                <option>Suisse</option>
                                <option>France</option>
                                <option>Allemagne</option>
                                <option>Etats-Unis</option>
                                <option>Canada</option>
                                <option>Australie</option>
                            </select>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Mot de passe</label>
                            <input type="password" class="form-control" name="password" required>
                        </div><!-- /.field-row -->

                        <div class="field-row">
                            <label>Confirmation de mot de passe</label>
                            <input type="password" class="form-control" name="password_ctrl" required>
                        </div><!-- /.field-row -->
                        <br>
                        <div class="field-row">
                            <div class="g-recaptcha" data-sitekey="6Lfxl1kUAAAAAKwLA6890P3Hlz35DeHM0aU76xZp"></div>
                        </div>

                        <br>
                        <div class="buttons-holder">
                            <button type="submit" name="submitpost" class="btn btn-default" style="background-color: lightgrey">S'inscrire</button>
                        </div><!-- /.buttons-holder -->

                    </form>
                </section><!-- /.register -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</main><!-- /.authentication -->
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>


<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v3.0&appId=594912934213550&autoLogAppEvents=1';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
