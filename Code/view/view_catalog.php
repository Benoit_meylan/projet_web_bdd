<?php
/**
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 17.05.2018
 * Time: 10:04
 */
ob_start();
?>
<head>
        <meta charset="utf-8">
        <title>Aurum - Bootstrap 4 Ecommerce Template</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <link rel="shortcut icon" href="images/favicon.ico">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-slider.min.css">

        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>

        <script src="js/bootstrap-slider.min.js"></script>

        <link rel="stylesheet" href="css/style.css">
    </head>


    <div id="topOfPage"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-3 sidebar-filter">
                <h3 class="mt-5 mb-5">Voir les <span class="primary-color"><?= $activeItem['countID'];?></span> Articles</h3>

                <!-- tri par categories -->
                <h6 class="text-uppercase">Categories</h6>
                <form class="form-control-lg" method="post" action="index.php?action=view_catalog">
                <div class="filter-checkbox">
                    <div class="custom-control custom-checkbox">
                        <input type="radio" class="custom-control-input" id="category-1" value="clothes" name="categorie">
                        <label class="custom-control-label" for="category-1">Vêtements</label>
                    </div>
                </div>
                <div class="filter-checkbox">
                    <div class="custom-control custom-checkbox">
                        <input type="radio" class="custom-control-input" id="category-2" value="perfumes" name="categorie">
                        <label class="custom-control-label" for="category-2">Parfums</label>
                    </div>
                </div>
                <div class="filter-checkbox">
                    <div class="custom-control custom-checkbox">
                        <input type="radio" class="custom-control-input" id="category-3" value="shoes" name="categorie">
                        <label class="custom-control-label" for="category-3">Chaussures</label>
                    </div>
                </div>

                <!-- tri par prix -->
                <div class="divider"></div>
                <h6 class="text-uppercase">Prix</h6>
                <div class="price-filter">
                    <input type="text" class="form-control" value="0" id="price-min">
                    <input type="text" class="form-control" value="10000" id="price-max">
                </div>
                <br />
                <br />
                <input id="ex2" type="text" class="slider " name="price" value="" data-slider-min="0" data-slider-max="10000" data-slider-step="10" data-slider-value="[0,10000]" />
                <div class="divider"></div>
                <!---------------------->
                    <input type="submit" class="btn btn-lg btn-full-width btn-primary mt-2" value="Mettre à jour">
            </div>
            </form>

            <div class="col-md-8 col-lg-9">
                <!-- produit -->
                <section class="products">
                    <div class="container">
                        <div class="row sorting mb-5">
                            <div class="col-12">
                            </div>
                        </div>

                        <!-- vetements -->
                        <div class="row">
                            <?php if (isset($clothes)) foreach ($clothes as $clothe):?>
                                <?php if($clothe['active'] == 1) :?>
                                    <div class="col-md-6 col-lg-4 col-product">
                                        <figure>
                                            <img class="rounded-corners img-fluid" src="<?=$clothe['lien_image1']?>" style="height: 200px; width: auto; margin-left: auto; margin-right: auto; display: block;">
                                            <figcaption>
                                                <div class="thumb-overlay"><a href="index.php?action=view_detail&id=<?=$clothe['idProduits']?>&category=<?=$clothe['category']?>" title="More Info">
                                                        <i class="fas fa-search-plus"></i>
                                                    </a></div>
                                            </figcaption>
                                        </figure>
                                        <h4 class="mb-1"><a href="index.php?action=view_detail&id=<?=$clothe['idProduits']?>&category=<?=$clothe['category']?>"><?=$clothe['nom']?></a></h4>
                                        <p><span class="emphasis"><?=$clothe['prix']?></span></p>
                                    </div>
                                <?php endif;?>

                            <?php endforeach;?>

                        </div>

                        <!-- chaussures -->
                        <div class="row">
                            <?php if (isset($shoes)) foreach ($shoes as $shoe):?>
                                <?php if($shoe['active'] == 1) :?>
                                    <div class="col-md-6 col-lg-4 col-product">
                                        <figure>
                                            <img class="rounded-corners img-fluid" src="<?=$shoe['lien_image1']?>" style="height: 200px; width: auto; margin-left: auto; margin-right: auto; display: block;">
                                            <figcaption>
                                                <div class="thumb-overlay"><a href="item.html" title="More Info">
                                                        <i class="fas fa-search-plus"></i>
                                                    </a></div>
                                            </figcaption>
                                        </figure>
                                        <h4 class="mb-1"><a href="index.php?action=view_detail&id=<?=$shoe['idProduits']?>&category=<?=$shoe['category']?>"><?=$shoe['nom']?></a></h4>
                                        <p><span class="emphasis"><?=$shoe['prix']?></span></p>
                                    </div>
                                <?php endif;?>

                            <?php endforeach;?>
                        </div>

                        <!-- parfums -->
                        <div class="row">
                            <?php if (isset($perfume)) foreach ($perfume as $item):?>
                                <?php if($item['active'] == 1) :?>
                                    <div class="col-md-6 col-lg-4 col-product">
                                        <figure>
                                            <img class="rounded-corners img-fluid" src="<?=$item['lien_image1']?>" style="height: 200px; width: auto; margin-left: auto; margin-right: auto; display: block;">
                                            <figcaption>
                                                <div class="thumb-overlay"><a href="item.html" title="More Info">
                                                        <i class="fas fa-search-plus"></i>
                                                    </a></div>
                                            </figcaption>
                                        </figure>
                                        <h4 class="mb-1"><a href="index.php?action=view_detail&id=<?=$item['idProduits']?>&category=<?=$item['category']?>"><?=$item['nom']?></a></h4>
                                        <p><span class="emphasis"><?=$item['prix']?></span></p>
                                    </div>
                                <?php endif;?>

                            <?php endforeach;?>
                        </div>

                        <div class="row sorting mb-5">
                            <div class="col-12"><a class="btn" href="#topOfPage"><i class="fas fa-arrow-up mr-2"></i> Back to top</a>
                                <div class="dropdown float-right">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

		<!-- Placed at the end of the document so the pages load faster -->
		<script src="js/jquery-3.1.1.min.js"></script>
		<script src="bootstrap/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>

		<script type="text/javascript">


			// Without JQuery
			var slider = new Slider('#ex2', {
});
			slider.on("slide", function(sliderValue) {
                document.getElementById("price-min").value = sliderValue[0];
            });
			slider.on("slide", function(sliderValue) {
                document.getElementById("price-max").value = sliderValue[1];
            });

		</script>

<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>