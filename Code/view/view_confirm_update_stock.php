<?php
/**
 * Created by PhpStorm.
 * User: MEYLANBenoit
 * Date: 04.06.2018
 * Time: 20:39
 */
$titre = "Hâpy - confirm modification stock";
// ouvre la mémoire tampon
ob_start();
?>
    <main id="authentication" class="inner-bottom-md">
        <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
            <div class="row">
                <div class="col-md-6">
                    <section class="section sign-in inner-right-xs">
                        <h3>Votre stock a bien été mis à jour !</h3>
                    </section>
                </div>
            </div>
        </div>
    </main>
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>