<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Hapy - Luxury</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="shortcut icon" href="images/favicon.ico">
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css">

    <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>

    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css_drag/style_drag.css">

    <script src='http://cdnjs.cloudflare.com/ajax/libs/dropzone/4.3.0/min/dropzone.min.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <style>

        .vertical-menu a {
            color: black; /* Black text color */
            display: block; /* Make the links appear below each other */
            padding: 12px; /* Add some padding */
            text-decoration: none; /* Remove underline from links */
        }
        .vertical-menu a:hover {
            background-color: #ccc; /* Dark grey background on mouse-over */
        }



    </style>
</head>
<body>
<section class="header text-center">
    <nav class="navbar navbar-expand-lg navbar-light navbar-custom">
        <div class="container"><a class="navbar-brand" href="index.php?action=home"><img src="assets/LogoHapy.png"><a></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-1" aria-controls="navbar-1" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse pull-xs-right justify-content-end" id="navbar-1">
                <ul class="navbar-nav mt-2 mt-md-0">
                    <li class="nav-item active"><a class="nav-link" href="index.php?action=home">Accueil <span class="sr-only">(current)</span></a></li>



                    <li class="nav-item"><a class="nav-link" href="index.php?action=view_contactUs">Contact</a></li>
                    <?php if(isset($_SESSION['utilisateur']['fkRoles'])):?>
                        <?php if($_SESSION['utilisateur']['fkRoles'] == 1):?>
                            <li class="nav-item"><a class="nav-link" href="index.php?action=my_account">Mon compte<img src='assets/my-account.png'></a></li>
                        <?php elseif($_SESSION['utilisateur']['fkRoles'] == 2) :?>
                            <li class="nav-item"><a class="nav-link" href="index.php?action=view_seller">Mon compte vendeur<img src='assets/my-account.png'></a></li>
                        <?php elseif($_SESSION['utilisateur']['fkRoles'] == 3) :?>
                            <li class="nav-item"><a class="nav-link" href="index.php?action=view_admin">Mon compte admin<img src='assets/my-account.png'></a></li>
                        <?php endif;?>
                    <?php else:?>
                        <?php if (isset($_SESSION['utilisateur'])) echo "<li class=\"nav-item\"><a class=\"nav-link\" href=\"index.php?action=my_account\">Mon compte</a></li>";
                        else echo "<li class=\"nav-item\"><a class=\"nav-link\" href=\"index.php?action=view_login\">Créer un compte/Se connecter</a></li>";
                        ?>
                    <?php endif;?>
                    <?php ?>

                      <?php  echo "<li class=\"nav-item\"><a class=\"nav-link\" href=\"index.php?action=view_catalog\">Tous les articles</a></li>"; ?>
                        <?php $count = 0;$_SESSION['total']=0;
                        if (isset($_SESSION['panier']))
                        {
                        foreach ($_SESSION['panier'] as $panier) {
                            $count++;
                            $_SESSION['total'] += ($panier['quantite'] * $panier['prix']);
                        }}?>

                    <li class="nav-item dropdown"><a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="fas fa-shopping-cart"></i> <span class="badge badge-pill badge-primary"><?= $count;?></span></a>
                        <div class="dropdown-menu dropdown-menu-right dropdown-cart" aria-labelledby="navbarDropdown">
                            <h6><?php if (isset($count)) echo $count;?> Items <span class="emphasis"><?php if(isset($_SESSION['total'])) echo $_SESSION['total']; ?> .- CHF</span></h6>
                            <div class="dropdown-divider"></div>
                            <ul class="shopping-cart-items">
                                <?php
                                if (isset($_SESSION['panier']))
                                {
                                foreach ($_SESSION['panier'] as $panier)
                                {
                                    echo "<li class=\"row\">
                                            <div class=\"col-3\">
                                                <img src=\"".$panier['lien_image1']."\" width=\"60\">
                                            </div>
                                            <div class=\"col-9\">
                                                <h6><a href=\"item.html\">".$panier['nom']."</a></h6>
                                                <span class=\"text-muted\">".$panier['quantite']."x</span>
                                                <span class=\"emphasis\">".$panier['prix']."</span></div>
                                            </li>";
                                }}
                                ?>
                            </ul>
                            <a href="index.php?action=view_cart" class="btn btn-lg btn-full-width btn-primary" style="margin: 0;">View Cart</a></div>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
</section>

<?php echo $contenu;?>

<div class="divider"></div>


    <div class="container">
        <div class="row">
            <div class="col-md-6 text-center text-md-left mt-2 mb-3 pt-1">
                <p class="copyright">Copyright &copy; Hapy. Theme by <a href="https://medialoot.com">Medialoot</a>.</p>
            </div>
            <div class="col-md-6 text-center text-md-right mb-4">
                <ul class="social">
                    <li><a href="#" title="Facebook"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="#" title="Twitter"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="#" title="Google+"><i class="fab fa-google"></i></a></li>
                    <li><a href="#" title="Dribbble"><i class="fab fa-dribbble"></i></a></li>
                    <li><a href="#" title="Instagram"><i class="fab fa-instagram"></i></a></li>
                    <div class="clear"></div>
                </ul>
            </div>
        </div>
    </div>



<!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery-3.1.1.min.js"></script>
<script src="bootstrap/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>

</body>
</html>