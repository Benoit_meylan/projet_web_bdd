<?php
/**
 * Created by PhpStorm.
 * User: MEYLANBenoit
 * Date: 21.06.2018
 * Time: 21:22
 */
// ouvre la mémoire tampon
ob_start();

?>


<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-12">
                <section class="section sign-in inner-right-xs">
                    <table class="table col-md-10">
                        <tr>
                            <td><b><u>nom du produit</u></b></td>
                            <td><b><u>prix</u></b></td>
                            <td><b><u>quantité</u></b></td>
                            <td><b><u>taille/pointure/contenance</u></b></td>
                        </tr>

                        <?php foreach ($product as $item):?>
                            <tr>
                                <td>
                                    <?=$item['nom']?>
                                </td>
                                <td>
                                    <?=$item['prix']?>
                                </td>
                                <td>
                                    <?=$item['quantite']?>
                                </td>
                                <td>
                                    <?=$item['pOption']?>
                                </td>
                            </tr>
                        <?php endforeach;?>
                    </table>
                </section>
            </div>
        </div>
    </div>
</main>



<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>
