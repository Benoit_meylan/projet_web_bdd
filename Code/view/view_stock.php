<?php
/**
 * Created by PhpStorm.
 * User: Benoit.MEYLAN
 * Date: 07.06.2018
 * Time: 10:26
 */

$titre = "hapy - stock";
// ouvre la mémoire tampon
ob_start();
// grab recaptcha library
?>
<script>
    $(function () {

        var cat = "<?=$item[0]['categorie']?>";
        var contenance = document.getElementById("contenance");
        var taille = document.getElementById("taille");
        var pointure = document.getElementById("pointure");


        switch(cat)
        {
            case "vetements":
                contenance.hidden = true;
                contenance.active = false;

                taille.hidden = false;
                taille.active = true;

                pointure.hidden = true;
                pointure.active = false
                break;
            case "parfum":
                contenance.hidden = false;
                contenance.active = true;

                taille.hidden = true;
                taille.active = false;

                pointure.hidden = true;
                pointure.active = false
                break;
            case "chaussures":
                contenance.hidden = true;
                contenance.active = false;

                taille.hidden = true;
                taille.active = false;

                pointure.hidden = false;
                pointure.active = true
                break;
        }
    });


</script>
<br>

<main id="authentication" class="inner-bottom-md">
    <div class="container" style="background-color: #FAEDD0; border-style: solid; border-radius: 5px; padding: 50px;" >
        <div class="row">
            <div class="col-md-6">
                <section class="section register inner-left-xs">
                    <h3 class="bordered">Gestion du stock pour : <?=$item[0]['nom']?></h3>

                    <form role="form" class="form-group" method="post" action="index.php?action=updateStock">
                        <table class="table">
                            <tr>
                                <th>option</th>
                                <th>stock actuel</th>
                                <th>stock à ajouter</th>
                            </tr>
                            <div class="field-row">
                                <?php foreach ($item as $value):?>
                                    <tr>
                                        <td><b><?=$value['options']?></b></td>
                                        <td><b><?=$value['stock']?></b></td>
                                        <td><input class="form-control" type="number" placeholder="stock à ajouter" name="stock[]"></td>
                                        <input type="hidden" value="<?=$value['options']?>" name="optionValue[]">
                                    </tr>
                                <?php endforeach;?>


                            </div><!-- /.field-row -->
                        </table>

                        <input type="hidden" value="<?=$item[0]['categorie']?>" name="category">
                        <input type="hidden" value="<?=$item[0]['idProduits']?>" name="id">

                        <input type="submit" class="btn btn-primary" value="Ajouter stock">
                    </form role="form" class="form-group" method="post" action="index.php?action=addUser">




                    <h3>Ajouter une option</h3>
                    <form method="post" class="form-group" action="index.php?action=add_option">
                        <div class="container">
                        <!-- options dynamiques des produits -->
                        <div id="contenance">
                            <label class="col-md-12 control-label">contenance (en ml)</label>
                            <div class="col-md-12 inputGroupContainer">
                                <select class="form-control" name="contenance" required">
                                <?php for ($i = 50; $i < 300;$i+=50):?>
                                    <option><?=$i?></option>
                                <?php endfor;?>
                                </select>
                            </div>
                            <br>
                        </div>

                        <div id="taille">
                            <label class="col-md-12 control-label">taille</label>
                            <div class="col-md-12 inputGroupContainer">
                                <select class="form-control" name="taille" required">
                                    <option value="XS">XS</option>
                                    <option value="S">S</option>
                                    <option value="M">M</option>
                                    <option value="L">L</option>
                                    <option value="XL">XL</option>
                                    <option value="XXL">XXL</option>
                                </select>
                            </div>
                            <br>
                        </div>

                        <div id="pointure">
                            <label class="col-md-12 control-label">pointure</label>
                            <div class="col-md-12 inputGroupContainer">
                                <select class="form-control" name="pointure" required>
                                    <?php for ($i = 16; $i < 47;$i++):?>
                                        <option><?=$i?></option>
                                    <?php endfor;?>
                                </select>
                            </div>
                            <br>
                        </div>

                        <input type="number" name="stock" placeholder="stock de base" class="form-control"><br>
                        <input type="hidden" value="<?=$item[0]['categorie']?>" name="category">
                        <input type="hidden" value="<?=$item[0]['idProduits']?>" name="id">

                        <input type="submit" class="btn btn-primary" value="Ajouter">
                        </div>
                    </form>
                </section><!-- /.register -->
            </div><!-- /.col -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</main><!-- /.authentication -->
<?php $contenu = ob_get_clean(); // Stocke la page dans la variable
require "layout.php";
?>



