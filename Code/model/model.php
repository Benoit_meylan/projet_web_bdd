<?php
/**
 * Created by PhpStorm.
 * User: Android
 * Date: 02.05.2018
 * Time: 21:14
 */

/**
 * - retourne la connexion à la base de donnée avec l'utilisateur ROOT
 * @return PDO
 */
function getBD()
{
    // Connexion au serveur MySql
    $connexion = new PDO("mysql:host=localhost;dbname=hapygest_web", "hapygest_web", "hapyisagod");


    //connexion local
   //$connexion = new PDO("mysql:host=localhost;dbname=hapy", "root", "");


    return $connexion;
}

/**
 * Inspiré d'une fonction fourni par Quentin Aellen et Dorian Nicklass
 * Permet de sécurisé les requêtes
 * @param $query - requête
 * @param array $args - arguments de la requête sous forme de tableau associatif
 * @return PDOStatement
 */
function sendQuery($query, $args = array())
{
    $dataBase = getBD();

    $prepareQuery = $dataBase->prepare($query);
    $prepareQuery->execute($args);

    $result = $prepareQuery;
    return$result;

}

/**
 * Ajoute un utilisateur à la base de donnée
 * @param $nom -nom de l'utilisateur
 * @param $prenom -prénom de l'utilisateur
 * @param $civilite -civilité de l'utilisateur (M. = homme, Mme = femme)
 * @param $mail -email de l'utilisateur
 * @param $passCrypt -mot de passe crypté de l'utilisateur
 * @param $nTel -n° de téléphone de l'utilisateur
 */
function new_user($nom, $prenom, $civilite, $mail, $passCrypt, $nTel)
{
    sendQuery("INSERT INTO `Utilisateurs` (`idUtilisateurs`, `fkRoles`, `nom`, `prenom`, `Civilite`, `eMail`, `motDePasse`, `nTelephone`) VALUES (NULL, '1', '$nom', '$prenom', '$civilite', '$mail', '$passCrypt', '$nTel')");
}

/**
 * Récupère les données de l'utilisateur à partir de son email
 * @param $mail -email de l'utilisateur permettant son identification
 * @return PDOStatement
 */
function get_user($mail)
{
    $user = sendQuery("SELECT idUtilisateurs, fkRoles, nom, prenom, civilite, eMail, motDePasse, nTelephone FROM Utilisateurs WHERE eMail = :mail", array("mail"=>$mail));

    return $user;
}

/**
 * Récupère le mail pour savoir si un utilisateur utilise déjà cet email
 * @param $mailCheck -mail à comparer
 * @return PDOStatement
 */
function compareMail($mailCheck)
{
    $resultat = sendQuery("SELECT eMail FROM Utilisateurs WHERE eMail = ':mailCheck'", array("mailCheck"=>$mailCheck));

    return $resultat;
}

/**
 * récupère les les id et les nom de la table catégorie
 * @return PDOStatement
 */
function getCategories()
{
    $resultats = sendQuery("SELECT * FROM Categories ORDER BY idCategories;");

    return $resultats;
}

/**
 * ajoute un produit dans la base de donnée
 * @param $category -clé étrangère de la table "categorie"
 * @param $name -nom du produit
 * @param $price -prix du produti
 * @param $desc -description du produit
 */
function add_Product($category, $name, $price, $desc, $active)
{
    sendQuery("INSERT INTO `Produits` (`idProduits`,`fkCategories`,`nom`,`prix`,`description`,`active`) VALUES (NULL,'$category', '$name', '$price', '$desc', '$active')");
}

/**
 * récupère tous les produits appartenant à la catégorie vetements
 * @return PDOStatement
 */
function getClothes($prixMin, $prixMax)
{

    $resultats = sendQuery("
            SELECT idProduits, Categories.nom as category, Produits.nom, prix, description, active, Images.lien_image1, Images.lien_image2, Images.lien_image3, Images.lien_image4
            FROM Produits 
            INNER JOIN Categories ON Produits.fkCategories = Categories.idCategories
            INNER JOIN Images ON Produits.idProduits = Images.fkProduits 
            WHERE prix BETWEEN :pMin AND :pMax AND Categories.nom = 'vetements'
            ORDER BY idProduits",array("pMin"=>$prixMin, "pMax"=>$prixMax));

    return $resultats;
}

/**
 * récupère tous les produits appartenant à la catégorie chaussures
 * @return PDOStatement
 */
function getShoes($prixMin,$prixMax)
{

    $resultats = sendQuery("
            SELECT idProduits, Categories.nom as category, Produits.nom, prix, description, active, Images.lien_image1, Images.lien_image2, Images.lien_image3, Images.lien_image4
            FROM Produits 
            INNER JOIN Categories ON Produits.fkCategories = Categories.idCategories
            INNER JOIN Images ON Produits.idProduits = Images.fkProduits
            WHERE prix BETWEEN :pMin AND :pMax AND Categories.nom = 'chaussures'
            ORDER BY idProduits",array("pMin"=>$prixMin, "pMax"=>$prixMax));

    return $resultats;

}

/**
 * récupère tous les produits appartenant à la catégorie parfum
 * @return PDOStatement
 */
function getPerfume($prixMin,$prixMax)
{

    $resultats = sendQuery("
            SELECT idProduits, Categories.nom as category, Produits.nom, prix, description, active, Images.lien_image1, Images.lien_image2, Images.lien_image3, Images.lien_image4
            FROM Produits 
            INNER JOIN Categories ON Produits.fkCategories = Categories.idCategories
            INNER JOIN Images ON Produits.idProduits = Images.fkProduits 
            WHERE prix BETWEEN :pMin AND :pMax AND Categories.nom = 'parfum'
            ORDER BY idProduits", array("pMin"=>$prixMin, "pMax"=>$prixMax));
    return $resultats;

}


/**
 * sélectionne tous les produits
 * @return PDOStatement
 */
function getProducts()
{
    $products = sendQuery("SELECT idProduits, Categories.nom as categorie, Produits.nom as nom, prix, active FROM Produits INNER JOIN Categories ON Produits.fkCategories = Categories.idCategories ", null);
    return $products;
}

/**
 * récupère les informations d'un produit
 * @param $id -id du produit
 * @param $category -catégorie du produit
 * @return PDOStatement
 */
function getItem($id, $category,$nb = 900)
{
    $select = null;
    $innerJoin = null;

    switch ($category)
    {
        case "vetements":
            $select = "Taille.taille as options, Taille_des_Produits.stock as stock";
            $innerJoin = " INNER JOIN Taille_des_Produits ON Taille_des_Produits.fkProduits = Produits.idProduits INNER JOIN Taille ON Taille_des_Produits.fkTaille = Taille.idTaille";
            break;
        case "chaussures":
            $select = "Pointure.pointure as options, Pointure_des_Produits.stock as stock";
            $innerJoin = " INNER JOIN Pointure_des_Produits ON Pointure_des_Produits.fkProduits = Produits.idProduits INNER JOIN Pointure ON Pointure_des_Produits.fkPointure = Pointure.idPointure";
            break;
        case "parfum":
            $select = "Contenance.contenance as options, Contenance_des_Produits.stock as stock";
            $innerJoin = " INNER JOIN Contenance_des_Produits ON Contenance_des_Produits.fkProduits = Produits.idProduits INNER JOIN Contenance ON Contenance_des_Produits.fkContenance = Contenance.idContenance";
            break;
    }

    $resultat = sendQuery("SELECT idProduits, Categories.nom as categorie, Produits.nom, prix, description, active,".$select.",Images.lien_image1, Images.lien_image2, Images.lien_image3, Images.lien_image4 
            FROM Produits 
            INNER JOIN Categories ON Produits.fkCategories = Categories.idCategories 
            ".$innerJoin."
            INNER JOIN Images ON Produits.idProduits = Images.fkProduits
            WHERE idProduits =:id ORDER BY Produits.idProduits", array("id"=>$id));

    return $resultat;

}

/**
 * récupère l'id du dernier produit ajouté
 * @return PDOStatement
 */
function getLastProductId()
{
    $resultat = sendQuery("SELECT MAX(idProduits) as id FROM Produits");

    return $resultat;
}


/**
 * récupère l'id d'une valeur de la table taille, contenance ou pointure
 * @param $value -nom de l'option
 * @param $name -nom de la table
 * @return PDOStatement
 */
function getOptionId($value, $name)
{
    $connexion = getBD();

    $idName = "id".ucfirst($name);
    $tableName = ucfirst($name);
    $fieldName = ucfirst($name);


    $resultats = sendQuery("SELECT $idName FROM $tableName WHERE $fieldName = '$value' ");

    return $resultats;
}

/**
 * ajoute une option (taille, pointure, contenance ) dans la table corespondante
 * @param $option
 * @param $product
 * @param $stock
 * @param $optionName
 */
function add_Stock($option, $product, $stock, $optionName)
{
    $tableName = ucfirst($optionName)."_des_Produits";


    sendQuery("INSERT INTO $tableName VALUES ('$option', '$product', '$stock')");
}

/**
 * ajoute le chemin des images d'un produit dans la BDD
 * @param $path -chemin des images (tableau)
 * @param $product -id du produit
 */
function add_pictures($path, $product)
{

    $values = ",'".$path[0]."'".",'".$path[1]."'".",'".$path[2]."'".",'".$path[3]."'";

    sendQuery("INSERT INTO `Images` (`fkProduits`, `lien_image1`, `lien_image2`, `lien_image3`, `lien_image4`) VALUES ('$product' $values)");


}

/**
 * récupère tous les utilisateurs
 * @return PDOStatement
 */
function getAllUser()
{
    $resultat = sendQuery("SELECT idUtilisateurs, fkRoles, Utilisateurs.nom, prenom, civilite, eMail, motDePasse, nTelephone,Roles.nom AS roles FROM Utilisateurs INNER JOIN Roles on Utilisateurs.fkRoles = Roles.idRoles");
    return $resultat;
}

/**
 * supprime un utilisateur
 * @param $idUser - id de l'utilisateur
 */
function del_user($idUser)
{
    sendQuery("DELETE FROM Adresses WHERE fkUtilisateurs = :idUser; DELETE FROM `Utilisateurs` WHERE `idUtilisateurs`=:idUser;", array("idUser"=>$idUser));

}


/**
 * désactive ou active un produit
 * @param $id - id du produit
 * @param $actif - état du produit (1, 0)
 */
function removeItem($id, $actif)
{
    sendQuery("UPDATE Produits SET active = $actif WHERE idProduits = :id", array("id"=>$id));
}

/**
 * modifie le stock d'un produit
 * @param $id - id du produit
 * @param $option - option du produit (taille, pointure)
 * @param $stock - stock à augmenter ou diminuer (attention pour diminuer le stock entrer un nombre négatif)
 * @param $optionId - id de l'option
 */
function updateStock($id, $option, $stock, $optionId)
{

    $optTable = ucfirst($option)."_des_Produits";
    $fkOption = "fk".ucfirst($option);

    sendQuery("UPDATE $optTable SET stock = stock + '$stock' WHERE fkProduits = :fkProduit AND $fkOption = :fkOption", array("fkProduit"=>$id, "fkOption"=>$optionId));
}

/**
 * change le rôle d'un utilisateur
 * @param $idUser - id de l'utilisateur
 * @param $role - futur rôle de l'utilateur
 */
function roles_augment($idUser,$role)
{

    sendQuery("UPDATE `Utilisateurs` SET `fkRoles`= $role WHERE `idUtilisateurs`=:idUser", array("idUser"=>$idUser));
}

/**
 * mets à jour un produit
 * @param $id - id du produit
 * @param $nom
 * @param $prix
 * @param $description
 */
function updateProduct($id, $nom, $prix, $description)
{
   sendQuery("UPDATE Produits SET nom = '$nom', prix = '$prix', description = '$description' WHERE idProduits = :id", array("id"=>$id));
}

function create_command($idUser)
{
    $date = date('Y-m-d H:i:s');

    sendQuery("INSERT INTO `Commande`(`fkUtilisateurs`, `dateCommande`) VALUES ($idUser,'$date')");
}

/**
 * @param $idCommand
 * @param $idProduit
 * @param $quantite
 */
function create_detail_command($idCommand,$idProduit,$quantite)
{

    sendQuery("INSERT INTO `Produits_des_Commandes`(`fkCommande`, `fkProduits`, `quantite`) VALUES ($idCommand,$idProduit,$quantite)");
}

/**
 * récupère toutes les options disponibles d'un produit
 * @param $id - id du produit
 * @param $category - catégorie du produit
 * @return null|PDOStatement - renvoie les options
 */
function getOptions($id, $category)
{

    $resultats = null;
    switch ($category)
    {
        case "vetements":
            $resultats = sendQuery("SELECT Taille.taille as options, stock FROM Taille INNER JOIN Taille_des_Produits ON Taille_des_Produits.fkTaille = Taille.idTaille WHERE Taille_des_Produits.fkProduits =:id", array("id"=>$id));
            break;
        case "chaussures":
            $resultats = sendQuery("SELECT Pointure.pointure as options, stock FROM Pointure INNER JOIN Pointure_des_Produits ON Pointure_des_Produits.fkPointure = Pointure.idPointure WHERE Pointure_des_Produits.fkProduits =:id", array("id"=>$id));
            break;
        case "parfum":
            $resultats = sendQuery("SELECT Contenance.contenance as options, stock FROM Contenance INNER JOIN Contenance_des_Produits ON Contenance_des_Produits.fkContenance = Contenance.idContenance WHERE Contenance_des_Produits.fkProduits =:id", array("id"=>$id));
            break;

    }


    return $resultats;
}

/**
 * ajoute une option à un produit comme une taille ou une pointure
 * @param $id - id du produit
 * @param $option - id de l'option
 * @param $stock - stock de base
 * @param $tblName - nom de la table
 */
function addOption($id, $option, $stock, $tblName)
{
    $tbl = ucfirst($tblName)."_des_Produits";
    $fk = "fk".ucfirst($tblName);

    sendQuery("INSERT IGNORE INTO $tbl (`$fk`, `fkProduits`, `stock`) VALUES ($option, $id, $stock)");

}

/**
 * ajoute le détail des commandes
 * @param $idCommande - Id de la commande
 * @param $idProduit - Id du produit
 * @param $quantite - Quantité du produit
 */
function addCommandDetail($idCommande, $idProduit, $quantite, $idOption)
{
    sendQuery("INSERT INTO `Produits_des_Commandes`(`fkCommande`, `fkProduits`, `quantite`, `pOption`) VALUES ($idCommande,$idProduit,$quantite,\"$idOption\")");
}

/**
 * récupére l'id d'une commande
 * @param $idUser - Id de l'utilisateur
 */
function getIdCommand($idUser)
{
    $resultats = sendQuery("SELECT MAX(`idCommande`) AS idCommande FROM `Commande` WHERE `fkUtilisateurs`= :id",array("id"=>$idUser));
    return $resultats;
}

/**
 * récupére toutes les commandes
 * @return PDOStatement
 */
function getCommands()
{
    $resultats = sendQuery("SELECT `idCommande`, idUtilisateurs ,`nom`, `dateCommande`, `adresse`, pays, ville, npa FROM Commande INNER JOIN Utilisateurs INNER JOIN Adresses ON Commande.fkUtilisateurs = Utilisateurs.idUtilisateurs AND Utilisateurs.idUtilisateurs = Adresses.fkUtilisateurs");
    return $resultats;
}

/**
 * ajoute une adresse
 * @param $address
 * @param $pays
 * @param $ville
 * @param $npa
 * @param $idUser
 */
function addAdress($address,$pays,$ville,$npa,$idUser)
{
    sendQuery("INSERT INTO `Adresses`(`adresse`, `pays`, `ville`, `npa`, `fkUtilisateurs`) VALUES (\"$address\",\"$pays\",\"$ville\",$npa,$idUser)");
}

/**
 * change le mot de passe d'un utilisateur
 * @param $mail
 * @param $nPass
 */
function updatePassword($mail, $nPass)
{
    sendQuery("UPDATE Utilisateurs SET motDePasse = '$nPass' WHERE eMail = :eMail", array("eMail"=>$mail));
}

/**
 * récuprère l'email d'un utilisateur
 * @param $idUser
 * @return PDOStatement
 */
function getPassword($idUser)
{
    $resultats = sendQuery("SELECT `motDePasse` FROM `Utilisateurs` WHERE `idUtilisateurs` = $idUser");
    return $resultats;
}

/**
 * récupére les détails d'un utilisateur spécifique
 * @param $idUser
 * @return string
 */

function getUserDetails($idUser)
{
    $resultats = sendQuery("SELECT `adresse`, `pays`, `ville`, `npa`, `Civilite`, `nom`, `nTelephone`, `prenom` FROM `Adresses` INNER JOIN Utilisateurs ON Adresses.fkUtilisateurs = Utilisateurs.idUtilisateurs WHERE Utilisateurs.idUtilisateurs = :idUser", array("idUser"=>$idUser));
    return $resultats;
}

/**
 * @param $adresse
 * @param $pays
 * @param $ville
 * @param $npa
 * @param $nom
 * @param $prenom
 * @param $civilite
 * @param $ntel
 * @param $idUser
 */
function updateAccount($adresse,$pays,$ville,$npa,$nom,$prenom,$civilite,$ntel,$idUser)
{
    sendQuery("UPDATE `Adresses` SET `adresse`=\"$adresse\",`pays`=\"$pays\",`ville`=\"$ville\",`npa`=$npa WHERE fkUtilisateurs = :idUser ",array("idUser"=>$idUser));

    sendQuery("UPDATE `Utilisateurs` SET `nom`=\"$nom\" ,`prenom`=\"$prenom\" ,`Civilite`=\"$civilite\",`nTelephone`=$ntel WHERE idUtilisateurs = :idUser ", array("idUser"=>$idUser));
}

/**
 * récupère tous les produits qui sont activés
 * @param $cat
 * @return PDOStatement
 */
function getActiveItemByCat($cat)
{
    switch ($cat)
    {
        case 'clothes': $catID = " AND fkCategories = 1"; break;

        case 'perfumes': $catID = " AND fkCategories = 2"; break;

        case 'shoes': $catID = " AND fkCategories = 3"; break;

        case 'All': $catID = ""; break;
    }

    $resultats = sendQuery("SELECT COUNT(`idProduits`) as countID FROM Produits WHERE `active` = 1 $catID");
    return $resultats;
}

/**
 * réupère les détails d'une commande
 * @param $idCommand -id de la commande
 * @return PDOStatement
 */
function getCommandDetail($idCommand)
{
    $resultats = sendQuery("SELECT Produits.nom as nom, Produits.prix as prix, Produits_des_Commandes.quantite as quantite, Produits_des_Commandes.pOption as pOption
                            FROM Produits_des_Commandes INNER JOIN Produits ON Produits.idProduits = Produits_des_Commandes.fkProduits
                            WHERE Produits_des_Commandes.fkCommande = :idCommand", array("idCommand"=>$idCommand));

    return $resultats;
}
